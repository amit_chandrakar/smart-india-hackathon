<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Green Route</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/swipebox.css">
    <link href="css/animate.min.css" rel="stylesheet">
  </head>
<body>
    <div class="main-body">
        <div class="container">
            <div class="row">

                <div class="main-page">

                    <aside class="main-navigation " >
                        <div class="main-menu">

                            <div class="menu-container wow slideInDown" data-wow-delay="0.1s">
                                <div class="block-keep-ratio block-keep-ratio-2-1 block-width-full home">                                    
                                    <a href="index.php" class="block-keep-ratio__content  main-menu-link">
                                        <span class="main-menu-link-text">
                                            HOME    
                                        </span>                                        
                                    </a>
                                </div>                                
                            </div>

                            <div class="menu-container wow slideInLeft" data-wow-delay="0.3s" >                                
                                <div class="block-keep-ratio  block-keep-ratio-1-1  block-width-half  pull-left  about-main">                                    
                                    <a href="#" class="main-menu-link about block-keep-ratio__content flexbox-center">
                                        <i class="fa fa-user fa-4x main-menu-link-icon"></i>
                                        ABOUT
                                    </a>                                    
                                </div>

                                <div class="block-keep-ratio  block-keep-ratio-1-1  block-width-half  pull-right  contact-main">
                                    <a href="contact.php" class="main-menu-link contact block-keep-ratio__content flexbox-center">
                                        <i class="fa fa-envelope-o fa-4x main-menu-link-icon"></i>
                                        CONTACT
                                    </a>                                
                                </div>    
                            </div>   

                            <div class="menu-container wow slideInUp" data-wow-delay="0.1s">
                                <div class="block-keep-ratio block-keep-ratio-1-1 block-keep-ratio-md-2-1 block-width-full gallery">                                    
                                    <a href="vision.php" class="main-menu-link  block-keep-ratio__content">
                                        <span class="main-menu-link-text">
                                            Our Vision    
                                        </span>                                            
                                    </a>                                    
                                </div>                                
                            </div>
                        </div> <!-- main-menu -->
                    </aside> <!-- main-navigation -->

                    <div class="content-main">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="banner-main-about margin-b-30">
                                    <div class="banner-main-about-text">
                                        <div style="margin-top:-9px;"><div class="heading">
                                            <h1>Green Route</h1>
                                            <p class="text-uppercase">Transport Made Eco Smart</p>
                                        </div>
                                        <dsiv class="desc2">
                                            <ul>
                                                <p>Green Route is the public bus transport provider for serving urban and sub-urban areas. Green Route is committed to provide quality, safe, reliable, clean and affordable travel. The testimony of its success lies in increasing passenger trips everyday by a wide range of customer base. In an effort to modernize its services for commuter comfort, Green Route strives to strengthen information systems and improve processes through introduction of intelligent technology solution, make capacity enhancement through infrastructure development, user-friendly interchange facilities, apart from its core activities, which includes fare structuring, route network optimization, planning and monitoring. Green Route’s stronghold in the area of public transport is a testimony to its adoption of sound Management, HR, Quality and Environmental policies and strong support from the Government and esteemed passengers.</p>
                                            </ul>
                                        </div>    
                            </div>                                                        
                        </div> <!-- .banner-main-about -->
       
                        <div class="row margin-b-30">
                            <div class="col-xs-12 col-sm-12 col-md-12 biliend">
                                <div class="banner-2-container">
                                    <div class="aenean">
                                        <h3>GreenRoute </h3>
                                        <h4>At Glance</h4>
                                    </div>    
                                </div>                                
                            </div>
                        </div>
                        <!-- <div class="row"> -->
                            <div class="about-last-container">
                                <!-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"> -->
                                   <div class="box bottom-main info-nav-container">
                                        <ul class="info info-ul">
                                    
                                            <li><a href="#" class="about_proin">No. Of Buses - 20</a></li>
                                            <li><a href="#" class="about_proin">No. Of Schedule - 20 </a></li>
                                            <li><a href="#" class="about_proin">No. Of Trips  -     20</a></li>
                                            <li><a href="#" class="about_proin">Bus Stations - 20</a></li>
                                            <li><a href="#" class="about_proin">Staff Employed - 20</a></li> 
                                            <li><a href="#" class="about_proin">Bus Staff Ratio - 2-</a></li>
                                            <li><a href="#" class="about_proin">Depots - 20</a></li>
                                            <li><a href="#" class="about_proin">Service kms - 20</a></li>
                                        </ul>
                                    </div>
                                <!-- </div> -->    
                                <!-- <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"> -->
                                    <div class="box bottom-main info-description-container">
                                        <div class="info">
                                            <div class="proin-title">
                                                <h3>A portal for Smart transport</h3>
                                                
                                            </div>                                       
                                            <img src="images/about-img-5.png" alt="Image" class="img-responsive">
                                            <p class="proin-right-text">This is a portal that will give you majorly the information about the city buses, routes, timings and Running status</p>
                                        </div> <!-- .info -->
                                    </div>
                                <!-- </div> -->
                            </div>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
           <footer class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer">
                    <p class="copyright">© 
                    
                    | Design: <a rel="nofollow" target="_parent">Spirit Busters</a></p>
                </div>    
            </footer>  <!-- .row -->     
        </div> <!-- .container -->
    </div> <!-- .main-body -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/wow.min.js"></script>
              <script>
              new WOW().init();
              </script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>