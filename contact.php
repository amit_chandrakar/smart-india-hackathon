<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Green Route</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/swipebox.css">
    <link href="css/animate.min.css" rel="stylesheet">
  </head>

<body class="contact-page">
    <div class="main-body">
        <div class="container">
            <div class="row">

                <div class="main-page">
                    
                    <aside class="main-navigation " >
                        <div class="main-menu">

                            <div class="menu-container wow slideInDown" data-wow-delay="0.1s">
                                <div class="block-keep-ratio block-keep-ratio-2-1 block-width-full home">                                    
                                    <a href="index.php" class="block-keep-ratio__content  main-menu-link">
                                        <span class="main-menu-link-text">
                                            HOME    
                                        </span>                                        
                                    </a>
                                </div>                                
                            </div>

                            <div class="menu-container wow slideInLeft" data-wow-delay="0.3s" >                                
                                <div class="block-keep-ratio  block-keep-ratio-1-1  block-width-half  pull-left  about-main">                                    
                                    <a href="about.php" class="main-menu-link about block-keep-ratio__content flexbox-center">
                                        <i class="fa fa-user fa-4x main-menu-link-icon"></i>
                                        ABOUT
                                    </a>                                    
                                </div>

                                <div class="block-keep-ratio  block-keep-ratio-1-1  block-width-half  pull-right  contact-main">
                                    <a href="contact.php" class="main-menu-link contact block-keep-ratio__content flexbox-center">
                                        <i class="fa fa-envelope-o fa-4x main-menu-link-icon"></i>
                                        CONTACT
                                    </a>                                
                                </div>    
                            </div>   

                            <div class="menu-container wow slideInUp" data-wow-delay="0.1s">
                                <div class="block-keep-ratio block-keep-ratio-1-1 block-keep-ratio-md-2-1 block-width-full gallery">                                    
                                    <a href="vision.php" class="main-menu-link  block-keep-ratio__content">
                                        <span class="main-menu-link-text">
                                            Our Vision    
                                        </span>                                            
                                    </a>                                    
                                </div>                                
                            </div>
                        </div> <!-- main-menu -->
                    </aside> <!-- main-navigation -->
                    <div class="content-main contact-content">
                        <div class="contact-content-upper">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="gallery_title">
                                        <h3>Contact</h3>
                                    
                                    </div>    
                                </div>                            
                            </div>
     
                            <div class="row">  
                                <div class="col-sm-12 col-md-6 contact_left">        
                                    <form class="form-horizontal" method="post" action="#">
          
                                        <div class="form-group">                                    
                                            <input type="text" class="form-control" id="name" name="name" placeholder="NAME..." value="">                                    
                                        </div>
          
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="email" name="email" placeholder="EMAIL..." value="">      
                                        </div>
          
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="subject" name="subject" placeholder="SUBJECT..." value="">        
                                        </div>
                                      
                                        <div class="form-group">
                                            <textarea class="form-control" rows="4" name="message" placeholder="MESSAGE..."></textarea>
                                        </div>
                                            
                                        <div class="form-group">
                                            <input id="submit" name="submit" type="submit" value="Send" class="btn view_more btn-submit">
                                        </div>            
                                    
                                    </form>    
                                </div> <!-- .contact-left -->
                            </div> <!-- .row -->
                        </div>

                    </div> <!-- .contact-content -->
                </div> <!-- .main-page -->
            </div> <!-- .row -->
 <footer class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer">
                    <p class="copyright">Copyright © 2016 Company Name 
                    
                    | Design: <a rel="nofollow" href="http://www.templatemo.com" target="_parent">Spirit Busters</a></p>
                </div>    
            </footer>  <!-- .row -->      
        </div> <!-- .container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/wow.min.js"></script>
              <script>
              new WOW().init();
              </script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>