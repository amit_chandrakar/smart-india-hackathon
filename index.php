<!DOCTYPE html>
<html lang="en">
  <head>
    <style type="text/css">

.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
}
.overlay:target {
  visibility: visible;
  opacity: 1;
}

.popup {
  margin: 80px auto;
  padding: 20px;
  background: #fff;
  border-radius: 5px;
  width: 60%;
  position: relative;
  transition: all 5s ease-in-out;
}

.popup h2 {
  margin-top: 0;
  color: #333;
  font-family: Tahoma, Arial, sans-serif;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
  color: #06D85F;
}
.popup .content {
  max-height: 30%;
  overflow: auto;
}

@media screen and (max-width: 700px){
  .popup{
    width: 70%;
  }
}
</style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Green Route</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/mycss.css" rel="stylesheet">
    <link rel="stylesheet" href="css/swipebox.css">
    <link href="css/animate.min.css" rel="stylesheet">
  </head>
  <body>
  <div class="main-body"> 
        <div class="container">

            <div class="row">               
                <div class="main-page">

                    <aside class="main-navigation">
                    <a href="ADMIN/index.php" style="color:white">Admin Login</a>
                        <div class="main-menu">

                            <div class="menu-container wow slideInDown" data-wow-delay="0.1s">
                                <div class="block-keep-ratio block-keep-ratio-2-1 block-width-full home">                                    
                                    <a href="index.php" class="block-keep-ratio__content  main-menu-link">
                                        <span class="main-menu-link-text">
                                            HOME    
                                        </span>                                        
                                    </a>
                                </div>                                
                            </div>

                            <div class="menu-container wow slideInLeft" data-wow-delay="0.3s">                                
                                <div class="block-keep-ratio  block-keep-ratio-1-1  block-width-half  pull-left  about-main">                                    
                                    <a href="about.php" class="main-menu-link about block-keep-ratio__content flexbox-center">
                                        <i class="fa fa-user fa-4x main-menu-link-icon"></i>
                                         ABOUT
                                    </a>                                    
                                </div>

                                <div class="block-keep-ratio  block-keep-ratio-1-1  block-width-half  pull-right  contact-main">
                                    <a href="contact.php" class="main-menu-link contact block-keep-ratio__content flexbox-center">
                                        <i class="fa fa-envelope-o fa-4x main-menu-link-icon"></i>
                                        CONTACT
                                    </a>                                
                                </div>    
                            </div>   

                            <div class="menu-container wow slideInUp" data-wow-delay="0.1s">
                                <div class="block-keep-ratio block-keep-ratio-1-1 block-keep-ratio-md-2-1 block-width-full gallery">                                    
                                    <a href="vision.php" class="main-menu-link  block-keep-ratio__content">
                                        <span class="main-menu-link-text">
                                            Our Vision    
                                        </span>                                            
                                    </a>                                    
                                </div>                                
                            </div>
                        </div> <!-- main-menu -->
                    </aside> <!-- main-navigation -->

<div class="content-main">
                        <div class="row margin-b-30 wow slideInRight" data-wow-delay="0.3s">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="banner-main-home ">                                
                                    <div class="banner-main-home-text">
                                       <div style="margin-top:-9px;"> <div class="heading" >
                                            <h1>GreenRoute</h1>
                                            <p class="text-uppercase">Transport Made Eco Smart</p>
                                        </div>
                                        </div>
                                    </br>
                                </br>
                
                                        <div class="desc">
                                            <form class="form-horizontal" method="post" action="#">
     
                                        <div class="form-group ">
                                        <label class="control-label">From:</label>
                                        <div class="input-group">
                                        <span class="input-group-addon" id="start-date"><span class="glyphicon glyphicon-map-marker"></span></span>


                                        <input type="text" class="form-control" id="search_box" name="s" style="color:#fff" placeholder="Location"> 
                                        </div>

                                       

                                        <div class="search_result" ></div>
                                        </div>
                                        
                                       

                                        <div class="form-group ">
                                        <label class="control-label">To:</label>
                                        <div class="input-group">
                                        <span class="input-group-addon" id="start-date"><span class="glyphicon glyphicon-map-marker"></span></span>
                                        <input type="text" class="form-control" id="search_box2" name="d" style="color:#fff"  placeholder="Location"> 
                                        </div>
                                        <div class="search_result2" ></div>
                                        </div>

                                    </br>
                                            
                                        <div class="form-group">
                                            <a class="btn btn-info btn-lg" id="serach_bus" href="#popup1">Search Bus</a>
                                        </div>            
                                    
                                     
                                        </div>
                                    </div>
                                </div>                        
                            </div>    
                        </div>
                    
                        <div class="row margin-b-30 wow slideInUp" data-wow-delay="1.5s">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="box london">
                                    <div class="box-icon">
                                        <img src="images/bus.png" alt="Image" class="img-responsive">
                                    </div>
                                    <div class="info float-container">
                                        <div class="col-sm-12 london-title">
                                        </br>
                                            <h3 class="text-uppercase">Search your Nearest Bus Stops</h3>
                                        </div>
                                         <div class="input-group">
                                        <span class="input-group-addon" id="start-date"><span class="glyphicon glyphicon-map-marker"></span></span>
                                        <input type="text" id="search_box3" class="form-control"placeholder="Location"> 
                                        </div>
                                         <br>
                                        <div class="col-sm-12 location-main"> 
                                            <div class="pull-left location">
                                            <div class="form-group">
                                            <a class="btn btn-info btn-lg"  id="serach_bus1" href="#popup2">Bus Stop</a>
                                      </form>
                                        </div>

                                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="box paris">
                                    <div class="box-icon">
                                        <img src="images/home-img-3.jpg" alt="Image" class="img-responsive">
                                    </div>
                                    <div class="info float-container">
                                        <div class="col-sm-12 london-title paris-title">
                                            <h3 class="text-uppercase">Single fare card & Schedule</h3>
                                        </div>
                                        <div class="desc">
                                            <form class="form-horizontal" method="post" action="#">
     
                                        <div class="form-group ">
                                        <label class="control-label">From:</label>
                                        <div class="input-group">
                                        <span class="input-group-addon" id="start-date"><span class="glyphicon glyphicon-map-marker"></span></span>
                                        <input type="text" class="form-control"placeholder="Location"> 
                                        </div>
                                        </div>
                                        
                                        <div class="form-group ">
                                        <label class="control-label">To:</label>
                                        <div class="input-group">
                                        <span class="input-group-addon" id="start-date"><span class="glyphicon glyphicon-map-marker"></span></span>
                                        <input type="text" class="form-control"placeholder="Location"> 
                                        </div>
                                        </div>

                                    
                                        <div class="form-group">
                                            <input id="submit" name="submit" type="submit" value="City Bus" class="btn view_more btn-submit">
 <a class="btn btn-info btn-lg"  id="serach_bus3" href="#popup3">Schedule</a>
                                        </div>            
                                    
                                    </form>    




                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- row -->
                    </div> <!-- .content-main -->
                </div> <!-- .main-page -->
            </div> <!-- .row -->  

      <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-left:180px">
                                <div class="box bottom-main wow slideInLeft" data-wow-delay="1.8">
                                    <div class="info float-container">
                                        <div class="col-sm-12 bottom-title" style="padding-left:-90px">
                                           <iframe src="https://www.google.com/maps/d/embed?mid=1bxFCL6DZ_HE56EHTMHVm_7VrPWM&hl=en_US" width="940" height="480"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- row -->

           <footer class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer">
                    <p class="copyright">Copyright © 2016 Company Name 
                    
                    | Design: Spirit Busters</a></p>
                </div>    
            </footer>  <!-- .row -->      
        </div> <!-- .container -->
    </div> <!-- .main-body -->
    <div id="popup1" class="overlay">
  <div class="popup">
     <div class="modal-header">
        <h4 class="modal-title">Route Details</h4>
      </div>
    <a class="close" href="#">&times;</a>
    <div class="content" id="route_table">
     
    </div>
  </div>
</div>


   <div id="popup2" class="overlay">
  <div class="popup">
     <div class="modal-header">
        <h4 class="modal-title">Route Details</h4>
      </div>
    <a class="close" href="#">&times;</a>
    <div class="content" id="route_table1">
     
    </div>
  </div>
</div>

 <div id="popup3" class="overlay">
  <div class="popup">
     <div class="modal-header">
        <h4 class="modal-title">Route Details</h4>
      </div>
    <a class="close" href="#">&times;</a>
    <div class="content" id="route_table3" >
     
    </div>
  </div>
</div>
 



 


 


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/wow.min.js"></script>
              <script>
              new WOW().init();
              </script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>


<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
    //------------------------------------
    $('.search_result').hide();
    $('#search_box').keyup(function(){
        var value = $(this).val();
        
        if(value != ''){
            $('.search_result').show();
            $.post('search.php',{value: value}, function(data){
                $('.search_result').html(data);
            });
        }else{
            
            $('.search_result').hide();
        }
    });
    //------------------------------------
    $('.search_result2').hide();
    $('#search_box2').keyup(function(){
        var value = $(this).val();
        var value_sour = $('#search_box').val();
        
        if(value != ''){
            $('.search_result2').show();
            $.post('search2.php',{value: value, value_sour: value_sour}, function(data){
                $('.search_result2').html(data);
            });
        }else{
            
            $('.search_result2').hide();
        }
    });
});


</script>

<!--search-->
<script type="text/javascript">
$(document).ready(function(){
    $('#serach_bus').click(function(){
        var value_s = $('#search_box').val();
        var value_d = $('#search_box2').val();
        
            $('.search_result').show();
            $.post('js_route_bus.php',{value_s: value_s,value_d: value_d}, function(data){
                $('#route_table').html(data);
            });
    

    });
});

</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#serach_bus1').click(function(){
        var value_s = $('#search_box3').val();
    
        
            $('.search_result').show();
            $.post('js_stop.php',{value_s: value_s}, function(data){
                $('#route_table1').html(data);
            });
    

    });
});

</script>

</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#serach_bus3').click(function(){
        
            $('.search_result').show();
            $.post('js_route.php',function(data){
                $('#route_table3').html(data);
            });
    

    });
});

</script>


  </body>
</html>