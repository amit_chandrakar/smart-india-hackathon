<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Green Route</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/admin.css" rel="stylesheet">
    <link rel="stylesheet" href="css/swipebox.css">
    <link href="css/animate.min.css" rel="stylesheet">
  </head>
  <body>
   <form class="form-horizontal" method="post" action="#">
  <nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Admin Panel</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user">&nbsp;</span>Hello Admin</a></li>
        <li class="active"><a title="View Website" href="#"><span class="glyphicon glyphicon-globe"></span></a></li>
        <li><a href="#">Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid">
  <div class="col-md-3">

    <div id="sidebar">
      <div class="container-fluid tmargin">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search..." />
          <span class="input-group-btn">
              <button class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
          </span>
        </div>
      </div>

      <ul class="nav navbar-nav side-bar">
        <li class="side-bar tmargin"><a  data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-list">&nbsp;</span>Dashboard</a></li>

        <li class="side-bar"><a data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-flag">&nbsp;</span>Buses</a>

        </li>
        <li class="side-bar"><a data-toggle="modal" data-target="#myModal2"><span class="glyphicon glyphicon-road">&nbsp;</span>Routes</a></li>
        <li class="side-bar">
          <a data-toggle="modal" data-target="#myModal3"><span class="glyphicon glyphicon-certificate">&nbsp;</span>Route Details</a></li>

        <li class="side-bar"> <a data-toggle="modal" data-target="#myModal4"><span class="glyphicon glyphicon-flag">&nbsp;</span>Stop Details</a></li>


      </ul>
    </div>

  </div>

</form>
  </body>







<div id="myModal"  class="modal fade" role="dialog" >
  <div class="modal-dialog modal-lg" style="height:auto ; width:auto;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Dashboard</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-9 animated bounce">
    <h1 class="page-header">Green Route</h1>
    <ul class="breadcrumb">
      <ul><span class="glyphicon glyphicon-home">&nbsp;</span>LogIn Time - 2:00</ul>
      <ul><span class="glyphicon glyphicon-home">&nbsp;</span>Date       - 2/7/17</ul>
      <ul><span class="glyphicon glyphicon-home">&nbsp;</span>IP Address       - 172.111.345</ul>
    </ul>
    <h4>Route number wise bus booking status</h4>
    <table class="table table-hover table th td">
      <thead>
        <th class="text-center">Route Number</th>
        <th>Bus Id</th>
        <th>Pollution</th>
        <th>Temperature</th>

      </thead>
      <tbody>

        <!-- START CONTENT END -->
        <tr>
          <td class="text-center ">1</td>
          <td><a href="https://www.#.html/">B1, </a>
          <a href="https://www.#.html/">B2,</a>
          <a href="https://www.#.html/">B3</a></td>
          <td class="text-center">20</td>
          <td>20C</td>
        </tr>
        <tr>
          <td class="text-center">2</td>
          <td ><a href="https://www.#.html/">B4,</a>
          <a href="https://www.#.html/">B7</a></td>
          <td class="text-center">20</td>
          <td>20C</td>
        </tr>
        <tr>

          <td class="text-center">3</td>
          <td ><a href="https://www.#.html/">B5,</a>
            <a href="https://www.#.html/">B6</a></td>
          <td class="text-center">20</td>
          <td>20C</td>
        </tr>
        <tr>
          <td class="text-center">4</td>
          <td><a href="https://www.#.html/">B9,</a>
            <a href="https://www.#.html/">B11</a></td>
          <td class="text-center">20</td>
          <td>20c</td>
        </tr>
       <!-- DUMP CONTENT END -->

      </tbody>

    </table>
  </div>
</div>
   

   
   <div align="center">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Sunday', 6],
          ['Monday', 2.2],
          ['Tuesday', 3],
          ['Wednesday', 4],
          ['Thusdday', 2],
          ['Friday', 6],
          ['Saturday', 9]
        ]);

        var options = {title: 'Pollution of First Week Of April'};

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
    <div id="piechart" style="width: 600px; height: 800px;"></div>
  </div>

</div>
</table>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">BUS</h4>
      </div>
      <div class="modal-body">
        
<div id="bus">
<table width="100%" class="table table-striped table-bordered table-hover">  
<tr>
<th>busID</th>
<th>vehicleNumber</th>
<th>regNumber</th>
</tr>

<tr id="row1">
<td id="name_row1">B1</td>
<td id="country_row1">cg4901</td>
<td id="age_row1">reg01</td>
<td>
<input type="button" id="edit_button1" value="Edit" class="edit" onclick="edit_row('1')">
<input type="button" id="update_button1" value="update" class="save" onclick="update_row('1')">
<input type="button" value="Delete" class="delete" onclick="delete_row('1')">
</td>
</tr>
<tr id="row2">
<td id="name_row2">B2</td>
<td id="country_row2">cg4902</td>
<td id="age_row2">reg02</td>
<td>
<input type="button" id="edit_button2" value="Edit" class="edit" onclick="edit_row('2')">
<input type="button" id="update_button2" value="update" class="save" onclick="update_row('2')">
<input type="button" value="Delete" class="delete" onclick="delete_row('2')">
</td>
</tr>

<tr id="row3">
<td id="name_row3">B3</td>
<td id="country_row3">cg4903</td>
<td id="age_row3">reg03</td>
<td>
<input type="button" id="edit_button3" value="Edit" class="edit" onclick="edit_row('3')">
<input type="button" id="update_button3" value="Save" class="save" onclick="save_row('3')">
<input type="button" value="Delete" class="delete" onclick="delete_row('3')">
</td>
</tr>

<tr>

  <td><input type="text" id="new_name"></td>
<td><input type="text" id="new_country"></td>
<td><input type="text" id="new_age"></td>
<td><input type="button" class="add" onclick="add_row();" value="Add Row"></td>
</tr>

</table>
</div>


</div>
      </div>
        <div class="modal-footer">
       <div id="piechart" style="width: 600px; height: 800px;"></div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

         <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Route</h4>
      </div>
      <div class="modal-body">
        
<div id="route">
<table width="100%" class="table table-striped table-bordered table-hover">
 
<tr>
<th>Route ID</th>
<th>Start Stop</th>
<th>End Stop</th>
<th>Route Name</th>

</tr>

<tr id="row1">
<td id="name_row1">1</td>
<td id="country_row1">Railway Station</td>
<td id="age_row1">Airport</td>
<td id="age_row1">Railway Station-Airport</td>
<td>
  <input type="button" id="edit_button1" value="Edit" class="edit" onclick="edit_row('1')">
<input type="button" id="update_button1" value="update" class="save" onclick="update_row('1')">
<input type="button" value="Delete" class="delete" onclick="delete_row('1')">
</td>
</tr>

<tr id="row2">
<td id="name_row2">2</td>
<td id="country_row2">Railway Station</td>
<td id="age_row2">Tatibandh</td>
<td id="age_row2">Railway Station-Tatibandh</td>
<td>
  <input type="button" id="edit_button1" value="Edit" class="edit" onclick="edit_row('2')">
<input type="button" id="update_button1" value="update" class="save" onclick="update_row('2')">
<input type="button" value="Delete" class="delete" onclick="delete_row('1')">
</td>

</tr>

<tr id="row3">
<td id="name_row3">3</td>
<td id="country_row3">Railway Station</td>
<td id="age_row3">Mantralaya</td>
<td id="age_row3">Railway Station-Mantralaya</td>
<td>
  <input type="button" id="edit_button1" value="Edit" class="edit" onclick="edit_row('3')">
<input type="button" id="update_button1" value="update" class="save" onclick="update_row('3')">
<input type="button" value="Delete" class="delete" onclick="delete_row('1')">
</td>

</tr>

<tr id="row4">
<td id="name_row4">4</td>
<td id="country_row4">Railway Station</td>
<td id="age_row4">Navagaon</td>
<td id="age_row3">Railway Station-Navagaon</td>
<td>
<input type="button" id="edit_button2" value="Edit" class="edit" onclick="edit_row('4')">
<input type="button" id="update_button2" value="update" class="save" onclick="update_row('4')">
<input type="button" value="Delete" class="delete" onclick="delete_row('4')">
</td>
</tr>



<tr>

  <td><input type="text" id="new_name"></td>
<td><input type="text" id="new_country"></td>
<td><input type="text" id="new_age"></td>
<td><input type="text" id="new_age"></td>
<td><input type="button" class="add" onclick="add_row();" value="Add Row"></td>
</tr>

</table>
</div>


</div>
      </div>


      <div class="modal-footer">
       <div id="piechart" style="width: 600px; height: 800px;"></div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>





<div id="myModal3" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

         <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Route Details</h4>
      </div>
      <div class="modal-body">
        
<div id="routedetails">
<table width="100%" class="table table-striped table-bordered table-hover">

<tr>
<th>Route ID</th>
<th>Stop ID</th>
<th>Stop Order</th>
<th>Distance</th>
<th>Fare</th>
<th>Arrival Timing</th>
</tr>

<tr id="row1">
<td id="name_row1">1</td>
<td id="name_row1">RLST</td>
<td id="country_row1">1A</td>
<td id="age_row1">0 Kms</td>
<td id="age_row1">0 Rs</td>
<td id="age_row1">8:00 AM | 10:00 AM | 12:30 PM | 2:00 PM | 4:00 PM</td>
<td>
  <input type="button" id="edit_button1" value="Edit" class="edit" onclick="edit_row('1')">
<input type="button" id="update_button1" value="update" class="save" onclick="update_row('1')">
<input type="button" value="Delete" class="delete" onclick="delete_row('1')">
</td>
</tr>

<tr id="row2">
<td id="name_row2">1</td>
<td id="name_row2">GC</td>
<td id="country_row2">1C</td>
<td id="age_row2">7 Kms</td>
<td id="age_row2">5 Rs</td>
<td id="age_row2">8:15 am | 10:15 AM | 12:45 PM |2:15 PM | 4:15 PM</td>
<td>
  <input type="button" id="edit_button1" value="Edit" class="edit" onclick="edit_row('2')">
<input type="button" id="update_button1" value="update" class="save" onclick="update_row('2')">
<input type="button" value="Delete" class="delete" onclick="delete_row('1')">
</td>

</tr>

<tr id="row3">
<td id="name_row3">1</td>
<td id="name_row3">MTHANA</td>
<td id="country_row3">1D</td>
<td id="age_row3">11 Kms</td>
<td id="age_row3">10 Rs</td>
<td id="age_row3">8:20 AM | 10:20 AM | 12:50 PM |2:20 PM | 4:20 PM</td>
<td>
  <input type="button" id="edit_button1" value="Edit" class="edit" onclick="edit_row('3')">
<input type="button" id="update_button1" value="update" class="save" onclick="update_row('3')">
<input type="button" value="Delete" class="delete" onclick="delete_row('1')">
</td>

</tr>




<tr>
  <td><input type="text" id="new_name"></td>
<td><input type="text" id="new_country"></td>
<td><input type="text" id="new_age"></td>


<td><input type="button" class="add" onclick="add_row();" value="Add Row"></td>
</tr>

</table>
</div>


</div>
      </div>


      <div class="modal-footer">
       <div id="piechart" style="width: 800px; height: 900px;"></div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>





<div id="myModal4" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

         <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Stop Details</h4>
      </div>
      <div class="modal-body">
        
<div id="routedetails">
<table width="100%" class="table table-striped table-bordered table-hover">  

<th>Stop ID</th>
<th>Stop Name</th>

</tr>

<tr id="row1">

<td id="name_row1">RLST</td>
<td id="country_row1">Railway Station</td>

<td>
  <input type="button" id="edit_button1" value="Edit" class="edit" onclick="edit_row('1')">
<input type="button" id="update_button1" value="update" class="save" onclick="update_row('1')">
<input type="button" value="Delete" class="delete" onclick="delete_row('1')">
</td>
</tr>

<tr id="row2">

<td id="name_row2">GC</td>
<td id="country_row2">Ghadi Chowk</td>

<td>
  <input type="button" id="edit_button2" value="Edit" class="edit" onclick="edit_row('2')">
<input type="button" id="update_button2" value="update" class="save" onclick="update_row('2')">
<input type="button" value="Delete" class="delete" onclick="delete_row('1')">
</td>

</tr>

<tr id="row3">

<td id="name_row3">MTHANA</td>
<td id="country_row3">Mahila Thana</td>

<td>
  <input type="button" id="edit_button3" value="Edit" class="edit" onclick="edit_row('3')">
<input type="button" id="update_button3" value="update" class="save" onclick="update_row('3')">
<input type="button" value="Delete" class="delete" onclick="delete_row('1')">
</td>
</tr>




<tr>

  <td><input type="text" id="new_name"></td>
<td><input type="text" id="new_country"></td>

<td><input type="button" class="add" onclick="add_row();" value="Add Row"></td>
</tr>

</table>
</div>


</div>
      </div>


      <div class="modal-footer">
       <div id="piechart" style="width: 800px; height: 900px;"></div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

<script>
function edit_row(no)
{
 document.getElementById("edit_button"+no).style.display="none";
 document.getElementById("save_button"+no).style.display="block";
  
 var name=document.getElementById("name_row"+no);
 var country=document.getElementById("country_row"+no);
 var age=document.getElementById("age_row"+no);
  
 var name_data=name.innerHTML;
 var country_data=country.innerHTML;
 var age_data=age.innerHTML;
  
 name.innerHTML="<input type='text' id='name_text"+no+"' value='"+name_data+"'>";
 country.innerHTML="<input type='text' id='country_text"+no+"' value='"+country_data+"'>";
 age.innerHTML="<input type='text' id='age_text"+no+"' value='"+age_data+"'>";
}

function update_row(no)
{
 var name_val=document.getElementById("name_text"+no).value;
 var country_val=document.getElementById("country_text"+no).value;
 var age_val=document.getElementById("age_text"+no).value;

 document.getElementById("name_row"+no).innerHTML=name_val;
 document.getElementById("country_row"+no).innerHTML=country_val;
 document.getElementById("age_row"+no).innerHTML=age_val;

 document.getElementById("edit_button"+no).style.display="block";
 document.getElementById("save_button"+no).style.display="none";
}

function delete_row(no)
{
 document.getElementById("row"+no+"").outerHTML="";
}

function add_row()
{
 var new_name=document.getElementById("new_name").value;
 var new_country=document.getElementById("new_country").value;
 var new_age=document.getElementById("new_age").value;
  
 var table=document.getElementById("data_table");
 var table_len=(table.rows.length)-1;
 var row = table.insertRow(table_len).outerHTML="<tr id='row"+table_len+"'><td id='name_row"+table_len+"'>"+new_name+"</td><td id='country_row"+table_len+"'>"+new_country+"</td><td id='age_row"+table_len+"'>"+new_age+"</td><td><input type='button' id='edit_button"+table_len+"' value='Edit' class='edit' onclick='edit_row("+table_len+")'> <input type='button' id='save_button"+table_len+"' value='Save' class='save' onclick='save_row("+table_len+")'> <input type='button' value='Delete' class='delete' onclick='delete_row("+table_len+")'></td></tr>";

 document.getElementById("new_name").value="";
 document.getElementById("new_country").value="";
 document.getElementById("new_age").value="";
}
  

</script>

  </html>