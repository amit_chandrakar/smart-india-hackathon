-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2017 at 12:53 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `greenroute`
--

-- --------------------------------------------------------

--
-- Table structure for table `bus`
--

CREATE TABLE IF NOT EXISTS `bus` (
  `busID` varchar(90) NOT NULL,
  `vehicleNumber` varchar(40) NOT NULL,
  `regNumber` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bus`
--

INSERT INTO `bus` (`busID`, `vehicleNumber`, `regNumber`) VALUES
('B1', 'cg4901', 'reg01'),
('B10', 'cg4910', 'reg10'),
('B11', 'cg4911', 'reg11'),
('B12', 'cg4912', 'reg12'),
('B13', 'cg4913', 'reg13'),
('B14', 'cg4914', 'reg14'),
('B15', 'cg4915', 'reg15'),
('B16', 'cg4916', 'reg16'),
('B17', 'cg4917', 'reg17'),
('B18', 'cg4918', 'reg18'),
('B19', 'cg4919', 'reg19'),
('B2', 'cg4902', 'reg02'),
('B20', 'cg4920', 'reg20'),
('B3', 'cg4903', 'reg03'),
('B4', 'cg4904', 'reg04'),
('B5', 'cg4905', 'reg05'),
('B6', 'cg4906', 'reg06'),
('B7', 'cg4907', 'reg07'),
('B8', 'cg4908', 'reg08'),
('B9', 'cg4909', 'reg09');

-- --------------------------------------------------------

--
-- Table structure for table `driver_login`
--

CREATE TABLE IF NOT EXISTS `driver_login` (
  `name` varchar(11) NOT NULL,
  `password` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `revroutedetails`
--

CREATE TABLE IF NOT EXISTS `revroutedetails` (
  `routeID` int(10) NOT NULL,
  `stopID` varchar(128) NOT NULL,
  `stopOrder` varchar(128) NOT NULL,
  `distanceSource` varchar(128) NOT NULL,
  `fare` varchar(128) NOT NULL,
  `Timing` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `revroutedetails`
--

INSERT INTO `revroutedetails` (`routeID`, `stopID`, `stopOrder`, `distanceSource`, `fare`, `Timing`) VALUES
(11, 'AIRP', '11A', '0 kms', '0 Rs', '8:30 AM|11:00 AM|1:00 PM|3:00 PM|5:00 PM'),
(11, 'MCAMP', '11B', '10 Kms', '10 Rs', '8:50 AM|11:20 AM|1:20 PM|3:20 PM|5:20 PM'),
(11, 'FMARKT', '11C', '15 Kms', '15 Rs', '9:00 AM|11:30 AM|1:30 PM|3:30 PM|5:30 PM'),
(11, 'POLINE', '11D', '20 kms', '20 Rs', '9:10 AM|11:40 AM|1:40 PM|3:40 PM|5:40 PM'),
(11, 'MTHANA', '11E', '24 kms', '25 Rs', '9:20 AM|11:50 AM|1:50 PM|3:50 PM|5:50 PM'),
(11, 'GC', '11F', '28 Kms', '30 Rs', '9:30 AM|12:00 PM|2:00 PM|4:00 PM|6:00 PM'),
(11, 'RLST', '11G', '35 kms', '35 Rs', '9:40 AM|12:10 PM|2:10 PM|4:10 PM|6:10 PM'),
(12, 'TATIBA', '12A', '0 Kms', '0 Rs', '9:10 AM | 11:10 AM | 1:40 PM | 3:10 PM | 5:10 PM'),
(12, 'BHMATA', '12B', '3 Kms', '5 Rs', '9:15 AM | 11:15 AM | 1:45 PM | 3:15 PM | 5:15 PM'),
(12, 'RKC', '12C', '8 Km', '10 Rs', '9:25 AM | 11:15 AM | 1:55 PM | 3:25 PM | 5:25 PM'),
(12, 'AMAP', '12D', '13 Kms', '15 Rs', '9:35 AM | 11:35 AM | 2:05 PM | 3:35 PM | 5:35 PM'),
(12, 'SARDCH', '12E', '18 Kms', '20 Rs', '9:50 AM | 11 :50 AM | 2:20 PM | 3:50 PM | 5:50 PM '),
(12, 'SHASCH', '12F', '23 Kms', '25 Rs', '10:10 AM | 12:10 PM | 2:40 PM | 4:10 PM | 6:10 PM'),
(12, 'RLST', '12G', '33 Kms', '30 Rs', '10:30 AM | 12:30 PM | 3:00 PM | 4:30 PM |6:30 PM');

-- --------------------------------------------------------

--
-- Table structure for table `route`
--

CREATE TABLE IF NOT EXISTS `route` (
`routeID` int(10) NOT NULL,
  `routeName` varchar(150) NOT NULL,
  `startStop` varchar(120) NOT NULL,
  `endStop` varchar(120) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `route`
--

INSERT INTO `route` (`routeID`, `routeName`, `startStop`, `endStop`) VALUES
(1, 'Railway Station - Airport', 'Railway Station', 'Airport'),
(2, 'Railway Station - Tatibandha', 'Railway Station', 'Tatibandh'),
(3, 'Railway Station - Mantralaya', 'Railway Station', 'Mantralaya'),
(4, 'Railway Station - Navagaon', 'Railway Station', 'Navagaon'),
(5, 'Pandri Bus Stand-Vidhan Sabha', 'Pandri Bus Stand', 'Vidhan Sabha'),
(6, 'Pandri Bus Stand-Kumhari', 'Pandri Bus Stand', 'Kumhari'),
(7, 'Pandri Bus Stand-Magneto Mall', 'Pandri Bus Stand', 'Magneto Mall'),
(8, 'Pandri Bus Stand-Veer Savarkar Nagar', 'Pandri Bus Stand', 'Veer Savarkar Nagar'),
(9, 'Railway Station-Pachpedi Naka', 'Railway Station', 'Pachpedi Naka'),
(10, 'Railway Station-AIIMS', 'Railway Station', 'AIIMS'),
(11, 'Airport-Railway Station', 'Airport', 'Railway Station'),
(12, 'Tatibandh-Railway Station', 'Tatibandh', 'Railway Station'),
(13, 'Mantralaya-Railway Station', 'Mantralaya', 'Railway Station'),
(14, 'Navagaon-Railway station', 'Navagaon', 'Railway Station'),
(15, 'Vidhan Sabha-Pandri Bus Stand', 'Vidhan sabha', 'Pandri Bus Stand'),
(16, 'Kumhari-Pandri Bus stand', 'Kumhari', 'Pandri Bus Stand'),
(17, 'Maagneto Mall-Pandri Bus Stand', 'Magneto Mall', 'Pandri Bus Stand'),
(18, 'Veer Savarkar Nagar-Pandri Bus stand', 'Veer Savarkar Nagar', 'Pandri Bus stand'),
(19, 'Pachpedi Naka-Railway Station', 'Pachpedi Naka', 'Railway Station'),
(20, 'AIIMS-Railway Station', 'AIIMS', 'Railway station');

-- --------------------------------------------------------

--
-- Table structure for table `routedetails`
--

CREATE TABLE IF NOT EXISTS `routedetails` (
  `routeID` int(20) NOT NULL,
  `stopID` varchar(128) NOT NULL,
  `stopOrder` varchar(128) NOT NULL,
  `Timing` varchar(128) NOT NULL,
  `distanceSource` varchar(128) NOT NULL,
  `fare` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `routedetails`
--

INSERT INTO `routedetails` (`routeID`, `stopID`, `stopOrder`, `Timing`, `distanceSource`, `fare`) VALUES
(1, 'RLST', '1A', '8:00 AM | 10:00 AM | 12:30 PM | 2:00 PM | 4:00 PM ', '0 Kms', '0 Rs'),
(1, 'GC', '1B', '8:15 am | 10:15 AM | 12:45 PM |2:15 PM | 4:15 PM', '7 Kms', '5 Rs'),
(1, 'MTHANA', '1C', '8:20 AM | 10:20 AM | 12:50 PM |2:20 PM | 4:20 PM', '11 Kms', '10 Rs'),
(1, 'POLINE', '1D', '8:30 AM | 10:30 AM | 1:00 PM |2:30 PM | 4:30 PM', '15 Kms', '15 Rs'),
(1, 'FMARKT', '1E', '8:40 AM | 10:40 AM | 1:10 PM |2:40 PM | 4:40 PM', '20 Kms', '20 Rs'),
(1, 'MCAMP', '1F', '8:50 AM | 10:50 AM | 1:20 PM |2:50 PM | 4:50 PM', '25 Kms', '25 Rs'),
(1, 'AIRP', '1G', '9:00 AM| 11:00 AM | 1:30 PM |3:00 PM | 5:00 PM', '35 Kms', '30 Rs'),
(2, 'RLST', '2A', '8:00 AM | 10:00 AM | 12:30 AM | 2:00 PM | 4:00 PM', '0 Kms', '0 Rs'),
(2, 'SHASCH', '2B', '8:20 AM | 10:20 AM | 12:50 AM | 2:20 PM | 4:20 PM', '10 Kms', '10 Rs'),
(2, 'SARDCH', '2C', '8:30 AM | 10:30 AM | 1 PM | 2:30 PM | 4:30 PM', '15 Kms', '15 Rs'),
(2, 'AMAP', '2D', '8:40 AM | 10:40 AM | 12:10 PM | 2:40 PM | 4:40 PM', '20 Kms', '20 Rs'),
(2, 'RKC', '2E', '8:50 AM  | 10:50 AM | 1:20 PM | 2:50 PM | 4:50 PM', '25 Kms', '25 Rs'),
(2, 'BHMATA', '2F', '9;00 AM | 11:00 AM | 1:30 PM | 3:00 PM |5:00 PM', '30 Kms', '30 Rs'),
(2, 'TATIBA', '2G', '9:06 AM | 11:06 AM | 1:36 PM | 3:06 PM | 5:06 PM', '33 Kms', '35 Rs');

-- --------------------------------------------------------

--
-- Table structure for table `stopdetails`
--

CREATE TABLE IF NOT EXISTS `stopdetails` (
  `stopID` varchar(128) NOT NULL,
  `stopName` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stopdetails`
--

INSERT INTO `stopdetails` (`stopID`, `stopName`) VALUES
('AIIMS', 'AIIMS'),
('AIRP', 'Airport'),
('AMAP', 'Amapara'),
('AMBU', 'Ambuja Mall'),
('ANANGR', 'Anand Nagar'),
('APMNGR', 'Anupam Nagar'),
('BENGCHWK', 'Bengali Hotel Chowk'),
('BHGTCH', 'Bhagat Singh Chowk'),
('BHMATA', 'Bharat Mata School'),
('BUDPRA', 'Budha Para'),
('DRGCLLG', 'Durga College'),
('FMARKT', 'Fruit Market'),
('GC', 'Ghadi Chowk'),
('GEC', 'GEC'),
('KUMH', 'Kumhari'),
('LPC', 'Lodhi Para Chowk'),
('MAGN', 'Magneto Mall'),
('MAHOBZR', 'Mahoba Bazar'),
('MANHAS', 'Mandir Hasaud'),
('MANTRA', 'Mantralaya'),
('MCAMP', 'Mana Camp'),
('MOUDPRA', 'Moudhapara'),
('MOVA', 'Mova'),
('MTHANA', 'Mahila Thana'),
('NAVAG', 'Navagaon'),
('PARSA', 'Parsada'),
('PBS', 'Pandri Bus Stand'),
('POLINE', 'Police Line'),
('RKC', 'RKC'),
('RKML', 'RK Mall'),
('RLST', 'Railway Station'),
('RSGRP', 'Ram Sagar Para'),
('SADDU', 'Saddu'),
('SARDCH', 'Sharda Chowk'),
('SDRBZR', 'Sadar Bazar'),
('SHASCH', 'Shastri Chowk'),
('SMTACOL', 'Samta Colony'),
('SNKRNGR', 'Shankar Nagar'),
('TATIBA', 'Tatibandh'),
('TELIBA', 'Telibandha'),
('VIMMRT', 'Vishal Mega Mart'),
('VIRCOL', 'Virdi Colony'),
('VRSKRNGR\r\n', 'Veer Savarkar Nagar'),
('VS', 'Vidhan Sabha');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bus`
--
ALTER TABLE `bus`
 ADD PRIMARY KEY (`busID`);

--
-- Indexes for table `revroutedetails`
--
ALTER TABLE `revroutedetails`
 ADD KEY `routeID` (`routeID`), ADD KEY `stopID` (`stopID`);

--
-- Indexes for table `route`
--
ALTER TABLE `route`
 ADD PRIMARY KEY (`routeID`);

--
-- Indexes for table `routedetails`
--
ALTER TABLE `routedetails`
 ADD KEY `routeID` (`routeID`), ADD KEY `stopID` (`stopID`), ADD KEY `stopID_2` (`stopID`), ADD KEY `stopID_3` (`stopID`), ADD KEY `stopID_4` (`stopID`), ADD KEY `stopID_5` (`stopID`), ADD KEY `stopID_6` (`stopID`), ADD KEY `stopID_7` (`stopID`), ADD KEY `stopID_8` (`stopID`);

--
-- Indexes for table `stopdetails`
--
ALTER TABLE `stopdetails`
 ADD PRIMARY KEY (`stopID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `route`
--
ALTER TABLE `route`
MODIFY `routeID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `revroutedetails`
--
ALTER TABLE `revroutedetails`
ADD CONSTRAINT `revroutedetails_ibfk_1` FOREIGN KEY (`routeID`) REFERENCES `route` (`routeID`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `revroutedetails_ibfk_2` FOREIGN KEY (`stopID`) REFERENCES `stopdetails` (`stopID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `routedetails`
--
ALTER TABLE `routedetails`
ADD CONSTRAINT `routedetails_ibfk_1` FOREIGN KEY (`routeID`) REFERENCES `route` (`routeID`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `routedetails_ibfk_2` FOREIGN KEY (`stopID`) REFERENCES `stopdetails` (`stopID`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `routedetails_ibfk_3` FOREIGN KEY (`stopID`) REFERENCES `stopdetails` (`stopID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
